# Alitwitter

## Description

A simple twitter app that support CRUD operation for tweet only.

## Dependencies

- Docker
  You must install docker first from this docker [doc](https://docs.docker.com/install/) to install it in your machine before you can build the docker image

## Environment Setup

- Ruby 2.6.5
- Rails 6.0.2.1
- Bundler 2.1.4

After installing ruby, then you must first install rails with this
```
gem install rails -v 6.0.2.1
```

Then, install all depedency using bundle in root of directory project.
```
bundle install
```

## Run Test

Using rspec & rubocop
```
bundle exec rake
```

To open the coverage result in browser
```
bundle exec rake coverage
```

## How to Run

### Docker

You can build the image of the app first with
```
docker build -t . alitwitter
```

and you can run with
```
docker run -p 3000:3000 alitwitter
```

### Local

To run this application, you must run this first
```
yarn

rake db:migrate
```

and then run the rails app with
```
rails s
```

It will open the default twitter page directly.
