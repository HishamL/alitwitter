require 'rails_helper'

RSpec.describe Tweet, type: :model do
  context 'given some string in content' do
    it 'should be valid tweet' do
      tweet = Tweet.new(content: 'some comment')
      expect(tweet).to be_valid
    end
  end

  context 'given some empty string in content' do
    it 'should be invalid tweet' do
      tweet = Tweet.new(content: '')
      expect(tweet).to be_invalid
    end
  end

  context 'given string length in content exceeds 140' do
    it 'should be invalid tweet' do
      content = 'This is some content that exceeds the 140 string length' \
                'which counts as invalid tweet and will not be saved to database'\
                'in which the string is so long that even the real twitter can\'t handle'
      tweet = Tweet.new(content: content)
      expect(tweet).to be_invalid
    end
  end
end
